React search
---
 
What has been used.

* React, * Redux, * Thunk, JWT authentication, Nodejs, Express, Mongodb, Mongoose

*** React to make components;

*** Redux to keep state;

*** Webpack to build the project;
 
 https://www.tutorialspoint.com/reactjs/reactjs_component_life_cycle.htm

 https://daveceddia.com/ajax-requests-in-react/
 
Usage
---
 
Start the development server with this command:
 
```
npm start
```
 
 
 
Setup
---
 
```
npm install
```
 
 
 
Compile
---
 
```
npm run compile

Run:


node server.js




**** Handy articles ***

https://www.lynda.com/React-js-tutorials/Authentication-setup/558648/597687-4.html?srchtrk=index%3a1%0alinktypeid%3a2%0aq%3aauthentication+react%0apage%3a1%0as%3arelevance%0asa%3atrue%0aproducttypeid%3a2

https://davidwalsh.name/react-authentication

https://developer.okta.com/blog/2017/03/30/react-okta-sign-in-widget

https://auth0.com/blog/adding-authentication-to-your-react-flux-app/



https://scotch.io/tutorials/authenticate-a-node-js-api-with-json-web-tokens


