var mongoose = require('mongoose');
var User = require('./user');

// var Message = mongoose.model('Message',{
//     message: {}
// });
//
// module.exports = Message;

module.exports = mongoose.model('Message', {
    message: {},
    user: {
        type: mongoose.Schema.ObjectId,
        ref: 'User'
    }
});