import React, { Component } from 'react';
import Header from '../components/header';
import Login from '../components/login';
import Register from '../components/register';

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as dataActions from "../actions/get-data";


import md5 from 'md5';

class App extends Component {
    constructor (props) {
        super();

        this.sendData = this.sendData.bind(this);
        this.getInputData = this.getInputData.bind(this);
        this.getList = this.getList.bind(this);
        this.handleLogin = this.handleLogin.bind(this);
        this.handleRegister = this.handleRegister.bind(this);
        this.handleFormChange = this.handleFormChange.bind(this);
        this.matchPassword = this.matchPassword.bind(this);
        this.loginUser = this.loginUser.bind(this);
        this.storeSession = this.storeSession.bind(this);
        this.getSessionToken = this.getSessionToken.bind(this);
        this.logout = this.logout.bind(this);

        this.state = {
            message: '',
            messages: [],
            loginUsername: '',
            loginEmail: '',
            loginPassword: '',
            registerUsername: '',
            registerEmail:'',
            registerPassword: '',
            registerPasswordConfirm: '',
            passwordConformation: true,
            registerPasswordNegativeMesage: 'Passwords don\'t match',
            error: '',
            isLoggedIn: false
        }
    }

    componentDidMount () {
        let token = this.getSessionToken();
        if(token) {
            this.setState({
                isLoggedIn: true
            });

            this.getList ();
        }
    }

    logout () {

        this.setState({
            isLoggedIn: false
        });

        sessionStorage.removeItem('access_token');
    }

    getList () {
        let token = this.getSessionToken();

        this.props.loadData(token)
            .then(() => {
                this.setState({
                    messages: this.props.data
                });
            })

            .catch((error) => {
                console.error(error);
            });

        // return fetch('/api/messages', {
        //     method: 'POST',
        //     headers: {
        //         'Accept': 'application/json',
        //         'Content-Type': 'application/json',
        //         //'Content-Type': 'text/html',
        //     },
        //     body: JSON.stringify({
        //         token
        //     })
        // })
        //     .then((response) => response.json())
        //     .then((responseJson) => {
        //         this.setState({
        //             messages: responseJson
        //         });
        //         return responseJson;
        //     })
        //     .then((responseJson) => {
        //         console.log(this.state.messages);
        //         //this.messageList();
        //     })
        //
        //     .catch((error) => {
        //         console.error(error);
        //     });
    }

    sendData () {

        let token = this.getSessionToken();
        let data = this.state.message;

        console.log("Send!!", data);

        fetch('/api/message', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'

            },
            body: JSON.stringify({
                message: data,
                token
            })
            //body: data
        })
            .then(function(response){
                console.log('send resp ', response);
                return response.json();
            }.bind(this))
            .then(function(data){
                this.getList();
            }.bind(this))
            .catch(function (resp) {
                console.log(resp);
            })
    }

    registerUser () {

        let data = {
            username: this.state.registerUsername,
            password: md5(this.state.registerPassword)
        };

        // fetch('/auth/register', {
        //     method: 'POST',
        //     headers: {
        //         'Accept': 'application/json',
        //         'Content-Type': 'application/json'
        //     },
        //     body: JSON.stringify({
        //         data
        //     })
        // })
        //
        // .then(function(response, err){
        //     return response.json();
        // })
        // .catch(function (resp) {
        //
        // })
        // .then(function(data){
        //     this.setState({
        //         error: data.errors.username.message
        //     });
        //     this.getList();
        // }.bind(this));


        this.props.registerUser(data)
            .then(()=>{

                let payload = this.props.newUser;

                if ( payload ) {

                    this.storeSession(payload.token);

                    if (payload.errors) {
                        this.setState({
                            error: payload.errors.username.message
                        });
                    }
                }

                this.setState({
                    isLoggedIn: true
                });

                this.getList();
            });
    }

    loginUser () {
        let data = {
            username: this.state.loginUsername,
            password: md5(this.state.loginPassword)
        };

        this.props.loginUser(data)
            .then(() => {

                let payload = this.props.user;

                if ( payload ) {
                    this.storeSession(payload.token);

                    if (payload.errors) {
                        this.setState({
                            error: payload.errors.username.message
                        });
                    }
                }

                this.setState({
                    isLoggedIn: true
                });

                this.getList();
            })

            .catch((error) => {
                console.error(error);
            });

        // fetch('/auth/login', {
        //     method: 'POST',
        //     headers: {
        //         'Accept': 'application/json',
        //         'Content-Type': 'application/json',
        //         //'Content-Type': 'text/html'
        //     },
        //     body: JSON.stringify(
        //         data
        //     )
        // })
        //
        // .then(function(response, err){
        //    console.log('LOGINNED ', response);
        //
        //     return response.json();
        // })
        // .catch(function (resp) {
        //
        //
        // })
        // .then(function(data){
        //     if ( data ) {
        //         console.log("data:::", data);
        //
        //         this.storeSession(data.token);
        //
        //         if (data.errors) {
        //             this.setState({
        //                 error: data.errors.username.message
        //             });
        //         }
        //     }
        //
        //     this.setState({
        //         isLoggedIn: true
        //     });
        //     this.getList();
        // }.bind(this))
    }

    storeSession (token) {
        if (typeof(Storage) !== "undefined") {
            sessionStorage.setItem("access_token", token);
        } else {
            // Sorry! No Web Storage support..
        }
    }

    getSessionToken () {
        if (typeof(Storage) !== "undefined") {
            return sessionStorage.getItem("access_token");
        } else {
            // Sorry! No Web Storage support..
        }
    }

    getInputData (event) {
        this.setState({
            message: event.target.value
        });
    }

    getInputUserData (event) {
        this.setState({
            message: event.target.value
        });
    }

    handleFormChange (e) {
        console.log("______: " + e.target.value);
        console.log("______: " + e.target.dataset.attr);

        let data = {};

        data[e.target.dataset.attr] = e.target.value;

        this.setState(
            data
        );
    }

    matchPassword () {
        console.log(this.state.registerPassword, "::::::::", this.state.registerPasswordConfirm);

       if (this.state.registerPassword != this.state.registerPasswordConfirm) {
           this.setState({
               passwordConformation: false
           });
       } else {
           this.setState({
               passwordConformation: true
           });

           this.registerUser();
       }
    }

    handleLogin () {
        this.loginUser();
    }

    handleRegister () {
        this.matchPassword();
    }

    render () {
        const isLoggedIn = this.state.isLoggedIn;
        let header = null;

        if(isLoggedIn == true) {
            header =  <Header logout={this.logout} />
        }

        return (
            <div className="messenger-container">

                {header}

                {isLoggedIn == false &&

                <div className="auth-block">
                    <div className="auth-error">{this.state.error}</div>

                    <Register handleFormChange={this.handleFormChange} handleRegister={this.handleRegister} passwordConformation={this.state.passwordConformation} registerPasswordNegativeMesage={this.state.registerPasswordNegativeMesage} />
                    <Login handleFormChange={this.handleFormChange} handleLogin={this.handleLogin} />

                </div>
                }
                { isLoggedIn == true &&
                    <div>
                        <input value={this.state.message} onChange={this.getInputData} onKeyUp={this.getInputData} type="text"/>
                        <button className="btn send" onClick={this.sendData}>Send</button>
                    </div>
                }
                <div>
                    <ul>
                        { isLoggedIn == true && this.state.messages &&
                            this.state.messages.map((el, index)=> {
                                return <div key={'nn'+index}>{el.message}</div>
                            })
                        }
                    </ul>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        data: state.data.data,
        user: state.user.user,
        newUser: state.newUser.user
    };
};

export default connect(
    mapStateToProps,
    dispatch => bindActionCreators(dataActions, dispatch)
)(App);
