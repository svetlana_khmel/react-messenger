import React from 'React';

const Header = (props) => {

    return (
        <div className="header">
            <a className="logout-link btn" onClick={props.logout}>Logout</a>
        </div>
    );
};

export default Header;

