import React from 'React';

const Header = (props) => {

    return (
        <div className="register-block">
            <h1>Register:</h1>
            <form>
                <div className="form-group">
                    <label htmlFor="username">Username:</label>
                    <input type="text" data-attr="registerUsername" className="form-control" id="username"
                           onKeyUp={props.handleFormChange}/>
                </div>
                {/*<div className="form-group">*/}
                {/*<label htmlFor="email">Email address:</label>*/}
                {/*<input type="email" data-attr="registerEmail" className="form-control" id="email" onKeyUp={this.handleFormChange} />*/}
                {/*</div>*/}

                <div className="form-group">
                    <label htmlFor="pwd">Password:</label>
                    <input type="password" data-attr="registerPassword" className="form-control" id="pwd"
                           onKeyUp={props.handleFormChange}/>
                </div>

                <div className="form-group">
                    <label htmlFor="pwd">Password confirm:</label>
                    <input type="password" data-attr="registerPasswordConfirm" className="form-control"
                           id="pwdConfirm" onKeyUp={props.handleFormChange}/>
                </div>

                {props.passwordConformation == false &&
                    <div className="tooltip">
                        {props.registerPasswordNegativeMesage}
                    </div>
                }

                {/*<div className="checkbox">*/}
                {/*<label><input type="checkbox"/> Remember me</label>*/}
                {/*</div>*/}
                <button type="button" className="btn btn-default" onClick={props.handleRegister}>Submit
                </button>
            </form>
        </div>
    );
};

export default Header;