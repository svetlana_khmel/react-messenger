import React from 'React';

const Login = (props) => {

    return (
        <div className="login-block">
            <h1>Login:</h1>

            <form>
                <div className="form-group">
                    <label htmlFor="login-username">Username:</label>
                    <input type="username" data-attr="loginUsername" className="form-control"
                           id="login-username" onKeyUp={props.handleFormChange}/>
                </div>
                {/*<div className="form-group">*/}
                {/*<label htmlFor="email">Email address:</label>*/}
                {/*<input type="email" data-attr="loginEmail" className="form-control" id="email" onKeyUp={this.handleFormChange} />*/}
                {/*</div>*/}
                <div className="form-group">
                    <label htmlFor="login-pwd">Password:</label>
                    <input type="password" data-attr="loginPassword" className="form-control" id="login-pwd"
                           onKeyUp={props.handleFormChange}/>
                </div>
                {/*<div className="checkbox">*/}
                {/*<label><input type="checkbox"/> Remember me</label>*/}
                {/*</div>*/}
                <button type="button" className="btn btn-default" onClick={props.handleLogin}>Submit</button>
            </form>

        </div>
    );
};

export default Login;