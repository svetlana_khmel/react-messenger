import { combineReducers } from 'redux';
import data from './data.js';
import user from './user.js';
import newUser from './registerUser';


const rootReducer =  combineReducers({
    data,
    user,
    newUser
});

export default rootReducer;