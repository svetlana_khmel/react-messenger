import data from '../data/data';

export function getData(data) {
    return {
        type: 'GET_MESSAGES',
        payload: data
    }
}

export function setUser(data) {
    return {
        type: 'LOGIN_USER',
        payload: data
    }
}

export function createUser(data) {
    return {
        type: 'REGISTER_USER',
        payload: data
    }
}

export function loadData(token) {
    return function (dispatch, getState) {
        return data.getMessages(token)
            .then((response)=>response.json())
            .then((data)=> dispatch(getData(data)))
            .catch((err)=> console.log(err));
    };
}

export function loginUser(userdata) {
    return function (dispatch, getState) {
        return data.loginUser(userdata)
            .then((response)=>response.json())
            .then((data)=> dispatch(setUser(data)))
            .catch((err)=> console.log(err));
    };
}

export function registerUser(userdata) {
    return function (dispatch, getState) {
        return data.registerUser(userdata)
            .then((response)=>response.json())
            .then((data)=> dispatch(createUser(data)))
            .catch((err)=> console.log(err));
    };
}





