import React from 'react';
import ReactDOM from 'react-dom';
import App from './containers/app';

import { Provider } from 'react-redux';
import configureStore from './configureStore';

const store = configureStore();

document.addEventListener('DOMContentLoaded', function() {
    ReactDOM.render(
        <Provider store={store}>
            <App />
        </Provider>,
        document.getElementById('mount')
    )
});