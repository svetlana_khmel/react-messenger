const dataUrl = '';
const loginUrl = '/auth/login';
const registerUrl = '/auth/register';
const getMessageUrl = '/api/messages';
const setMessageUrl = '/api/message';


const data = {
    getMessages (token) {
        console.log("___ ", token);

        return fetch(getMessageUrl, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                //'Content-Type': 'text/html',
            },
            body: JSON.stringify({
                token
            })
        });
    },

    setMessages (data) {
        return fetch(setMessageUrl)
    },

    loginUser (data) {
        return fetch(loginUrl, {
            method: 'POST',
                headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                //'Content-Type': 'text/html'
            },
            body: JSON.stringify(
                data
            )
        });
    },

    registerUser (data) {
        return fetch(registerUrl, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                data
            })
        })
    }
};

export default data;
